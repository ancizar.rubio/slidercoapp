from django.db import models

# Create your models here.

class Empresa(models.Model):
    nombre = models.CharField(max_length=100, null=True, verbose_name="Operador")
    imagen = models.ImageField(verbose_name="imagen", upload_to="Operador", height_field=None, width_field=None, max_length=None)
    color = models.CharField(verbose_name="Color", blank=True, max_length=100)
    descripcion = models.TextField(max_length=500, blank=True, null=True, verbose_name="Descripcion")
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edicion')
    
    class Meta:
        verbose_name = "operador"
        verbose_name_plural = "operadores"
        ordering = ['-created']
        
    def __str__(self):
        return self.nombre