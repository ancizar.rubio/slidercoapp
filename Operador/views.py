from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import viewsets, views
from rest_framework.response import Response
from .models import Empresa
from .serializers import EmpresaSerializer
from Operador.models import Empresa as EmpresaModel

# Create your views here.
    
class Empresa(views.APIView):
    def get(self, request):
            queryset = EmpresaModel.objects.all()
            serializer = EmpresaSerializer(queryset, many=True)
        
            return Response(serializer.data)