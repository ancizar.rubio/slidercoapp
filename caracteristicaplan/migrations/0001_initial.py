# Generated by Django 3.0.3 on 2020-02-29 04:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('plan', '0010_delete_caracteristica'),
    ]

    operations = [
        migrations.CreateModel(
            name='Caracteristica',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(help_text='respuesta segun pregunta anterior', max_length=100, verbose_name='Descripcion')),
                ('opcion_base', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='opcionBaseCaracteristica', to='plan.Planes', verbose_name='Opcion')),
            ],
            options={
                'verbose_name': 'opcionCaracteristica',
                'verbose_name_plural': 'opcionCaracteristicas',
                'ordering': ['id'],
            },
        ),
    ]
