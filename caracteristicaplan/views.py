from django.shortcuts import render
from rest_framework.views import  APIView
from caracteristicaplan.serializers import CaracteristicaSerializer
from rest_framework.response import Response
from caracteristicaplan.models import Caracteristica as CaracteristicaModel

# Create your views here.

class Caracteristica(APIView):
    
    def get(self, request):
        queryset = CaracteristicaModel.objects.all()
        serializer = CaracteristicaSerializer(queryset, many=True) 
    
        return Response(serializer.data)      
        