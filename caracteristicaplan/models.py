from django.db import models
from plan.models import Planes

# Create your models here.

class Caracteristica(models.Model):
    opcion_base = models.ForeignKey(Planes, verbose_name="Opcion", related_name="opcionBaseCaracteristica", on_delete=models.CASCADE)
    descripcion = models.CharField(verbose_name="Descripcion", max_length=100, help_text="respuesta segun pregunta anterior")
    
    #def __str__(self):
     #   return str(self.opcionBase) + ' ,' + str(self.descripcion)
    
    class Meta:
        verbose_name="opcionCaracteristica"
        verbose_name_plural = "opcionCaracteristicas"
        ordering = ['id']