from rest_framework import serializers
from .models import Caracteristica


class CaracteristicaSerializer(serializers.ModelSerializer):
    #descripcion = serializers.StringRelatedField(many=True)
    
    class Meta:
        model = Caracteristica
        fields = '__all__'
         