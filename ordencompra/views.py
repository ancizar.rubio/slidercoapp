from django.shortcuts import render
from rest_framework import request, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from ordencompra.serializers import CompraSerializer
from ordencompra.models import Compra as CompraModel
from django.db.migrations import serializer


# Create your views here.

#class Compra(APIView):
@api_view(['GET', 'POST'])
def Compra_list(request):
    if request.method == 'GET':
        queryset = CompraModel.objects.all()
        serializer = CompraSerializer(queryset, many=True)
        return Response(serializer.data)
    
    elif request.method == 'POST':
        serializer = CompraSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
        
        