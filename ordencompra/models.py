from django.db import models
from plan.models import Planes
from usuario.models import User

# Create your models here.

class Compra(models.Model):
    pedido = models.CharField(verbose_name="Nombre", blank=True, null=True, max_length=50)
    usuario = models.ForeignKey(User, verbose_name="Usuario", on_delete=models.CASCADE)
    articulo = models.ForeignKey(Planes, verbose_name="Articulo", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edicion')
    
    class Meta:
        verbose_name = "Compra"
        verbose_name_plural = "Compras"
        ordering = ['id']
        
    def __str__(self):
        return self.pedido if self.pedido else 'nueva orden'