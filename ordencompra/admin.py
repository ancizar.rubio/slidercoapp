from django.contrib import admin
from .models import Compra

# Register your models here.
class CompraAdmin(admin.ModelAdmin):
    readonly_fields = ('created','updated')

admin.site.register(Compra, CompraAdmin)