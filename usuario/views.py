from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework_jwt.utils import jwt_payload_handler
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from django.contrib.auth.hashers import make_password
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth import authenticate, logout
#from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render,redirect
from usuario.serializers import UserSerializer
from usuario.models import User
from django.conf import settings
from datetime import datetime
import jwt
#from django.contrib.auth.forms import AuthenticationForm
#from django.urls import reverse_lazy
#from rest_framework import generics
#from django.http import HttpResponseRedirect
#from django.template.base import Token
#from django.utils.decorators import method_decorator
#from django.views.generic.edit import FormView
#from django.contrib.auth.forms import AuthenticationForm
#from rest_framework.authentication import TokenAuthentication
#from django.views.decorators.csrf import csrf_protect
#from django.views.decorators.cache import never_cache



# Create your views here.

class CreateUserAPIview(APIView):
    # Allow any user (authenticated or not) to access this url 
    permission_classes = (AllowAny,)
 
    def post(self, request):
        user = request.data
        serializer = UserSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
class UserRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    
    # Allow only authenticated users to access this url
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        # serializer to handle turning our `User` object into something that
        # can be JSONified and sent to the client.
        serializer = self.serializer_class(request.user)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer_data = request.data.get('user', {})

        serializer = UserSerializer(
            request.user, data=serializer_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([AllowAny, ])
def authenticate_user(request):
 
    try:
        email = request.data['email']
        password = request.data['password']
 
        user = User.objects.get(email=email, password=password)
        if user:
            try:
                payload = jwt_payload_handler(user)
                token = jwt.encode(payload, settings.SECRET_KEY)
                user_details = {}
                user_details['name'] = "%s %s" % (
                    user.first_name, user.last_name)
                user_details['token'] = token
                user_logged_in.send(sender=user.__class__,
                                    request=request, user=user)
                return Response(user_details['token'], status=status.HTTP_200_OK)
 
            except Exception as e:
                raise e
        else:
            res = {
                'error': 'can not authenticate with the given credentials or the account has been deactivated'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
    except KeyError:
        res = {'error': 'please provide a email and a password'}
        return Response(res)
    
class Logout(APIView):
    def get(self,request, format = None):
        request.User.auth_token.delete()
        logout(request)
        return Response(status = status.HTTP_200_OK)
    

#class Login(FormView):
 #   template_name = "login.html"
  #  form_class = AuthenticationForm
   # success_url = reverse_lazy('usuario:usuario-list')

    #@method_decorator(csrf_protect)
    #@method_decorator(never_cache)
    #def dispatch(self,request,*args,**kwargs):
     #   if request.user.is_authenticated:
      #      return HttpResponseRedirect(self.get_success_url())
       # else:
        #    return super(Login,self).dispatch(request,*args,*kwargs)

    #def form_valid(self,form):
     #   email = authenticate(email = form.cleaned_data['email'], password = form.cleaned_data['password'])
      #  token,_ = Token.objects.get_or_create(user = User)
       # if token:
        #    login(self.request, form.get_user())
         #   return super(Login,self).form_valid(form)

#class Logout(APIView):
 #   def get(self,request, format = None):
  #      request.user.auth_token.delete()
   #     logout(request)
    #    return Response(status = status.HTTP_200_OK)