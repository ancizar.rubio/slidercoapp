from django.urls import path
from .views import CreateUserAPIview
from .views import authenticate_user
from usuario.views import UserRetrieveUpdateAPIView
#from usuario.views import Login, Logout,
from usuario.views import Logout

 
urlpatterns = [
    path('create/',CreateUserAPIview.as_view()),
    path('update/', UserRetrieveUpdateAPIView.as_view()),
    path('obtain_token/', authenticate_user),
    path('logout/', Logout.as_view()),
]