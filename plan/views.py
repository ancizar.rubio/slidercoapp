from django.shortcuts import render
from rest_framework import views
from rest_framework.views import  APIView
from rest_framework.response import Response
from plan.serializers import PlanesSerializer
from plan.models import Planes as PlanesModel
#from plan.models import Caracteristica as CaracteristicaModel
from rest_framework.permissions import IsAuthenticated


# Create your views here.

class Planes(APIView):
    
    permission_classes = (IsAuthenticated,)
    serializer_class = PlanesSerializer
        
        
    def get(self, request, service):        
        queryset = PlanesModel.objects.all().filter(service=service)
        serializer = PlanesSerializer(queryset, many=True) 
    
        return Response(serializer.data)
    
    
#class Caracteristica(views.APIView):
    
 #   def get(self, request):
  #      queryset = CaracteristicaModel.objects.all()
   #     serializer = CaracteristicaSerializer(queryset, many=True)
        
    #    return Response(serializer.data)