# Generated by Django 3.0.3 on 2020-02-29 06:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('unidadMedida', '0001_initial'),
        ('plan', '0013_remove_planes_unidadmedida'),
    ]

    operations = [
        migrations.AddField(
            model_name='planes',
            name='unidadmedida',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='unidadMedida.Velocidad', verbose_name='Unidad de medidad'),
            preserve_default=False,
        ),
    ]
