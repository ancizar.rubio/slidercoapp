from django.db import models
from TipoServicio.models import TipoServicio
from Operador.models import Empresa
from TipoPlanes.models import servicio
import plan
from unidadMedida.models import Velocidad


# Create your models here.

class Planes(models.Model):
    Tplan = models.ForeignKey(servicio, verbose_name="Tipo_plan", on_delete=models.CASCADE)
    nombre = models.CharField(blank=True, verbose_name="Nombres", max_length=100)
    capacidad = models.CharField(verbose_name="Capacidad", max_length=50)
    unidadmedida = models.ForeignKey(Velocidad, verbose_name="Unidad de medidad", on_delete=models.CASCADE)
    oferta = models.CharField(blank=True, null=True, verbose_name="Oferta",max_length=100)
    precio = models.CharField(blank=False, max_length=100, verbose_name="Precio")
    service = models.ManyToManyField(TipoServicio, verbose_name="Servicio")
    firma = models.ManyToManyField(Empresa, verbose_name="Operadores")
    tyc = models.TextField(help_text="Terminos y Condiciones", max_length=400, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edicion')
    
    class Meta:
        verbose_name = "Planes"
        verbose_name_plural = "Planes"
        ordering = ['-created']
        
    def __str__(self):
        return self.nombre
    

class Versus(models.Model):
    titulo = models.CharField(blank=True, null=True, verbose_name="Titulo", max_length=50)
    planes1 = models.ManyToManyField(Planes, related_name="Versus_planes1", blank=True, verbose_name="planes1")
    planes2 = models.ForeignKey(Planes, related_name="Versus_planes2", verbose_name="planes2", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edicion')
    
    class Meta:
        verbose_name = "versus"
        verbose_name_plural = "versus"
        ordering = ['-created']
        
    def __str__(self):
       return self.titulo