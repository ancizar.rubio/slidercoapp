from rest_framework import serializers
from plan.models import Planes
from TipoServicio.models import TipoServicio
from TipoServicio.serializers import TipoServicioSerializer
from Operador.serializers import EmpresaSerializer
from caracteristicaplan.serializers import CaracteristicaSerializer


class PlanesSerializer(serializers.ModelSerializer):
    Tplan = serializers.SerializerMethodField()
    unidadmedida = serializers.SerializerMethodField()
    service = TipoServicioSerializer(many=True)
    firma = EmpresaSerializer(many=True)
    opcionBaseCaracteristica = CaracteristicaSerializer(many=True)
    #descripcion = CaracteristicaSerializer(many=True)
    #queryset = TipoServicio.objects.all()
    #data = serializers.serialize(queryset, fields=('nombre'))
    
    def get_Tplan(self, obj): 
         return obj.Tplan.nombre
     
    def get_unidadmedida(self, obj): 
         return obj.unidadmedida.velocidad
   
    class Meta:
        model = Planes
        fields = ['id','Tplan', 'nombre', 'capacidad', 'unidadmedida', 'oferta','precio',
                   'service', 'firma', 'tyc', 'opcionBaseCaracteristica']
    
    
             

class VersusSerializer(serializers.ModelSerializer):
      
     class Meta:
         model = Planes
         fields = ['titulo', 'planes1', 'planes2']