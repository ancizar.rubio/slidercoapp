from django.contrib import admin
from .models import Planes, Versus
from caracteristicaplan.models import Caracteristica

# Register your models here.

class CaracteristicaInline(admin.TabularInline):
    model = Caracteristica

class PlanesAdmin(admin.ModelAdmin):
    list_filter = ('nombre','Tplan')
    inlines = [CaracteristicaInline,]
  
admin.site.register(Planes, PlanesAdmin)


class VersusAdmin(admin.ModelAdmin):
    readonly_fields = ('created','updated')

admin.site.register(Versus, VersusAdmin)