from django.contrib import admin
from django.urls import path, include
from usuario.views import Logout
from rest_framework import routers
from django.conf import settings

router = routers.DefaultRouter()


urlpatterns = [
     path('',include(router.urls)),
    path('admin/', admin.site.urls),
    path('usuario/', include('usuario.urls')),
    path('TipoServicio/',include('TipoServicio.urls')),
    path('Operador/',include('Operador.urls')),
    path('plan/planes/',include('plan.urls')),
    path('caracteristicaplan/',include('caracteristicaplan.urls')),
     path('ordencompra/',include('ordencompra.urls')),
    
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)