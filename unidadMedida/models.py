from django.db import models

# Create your models here.

class Velocidad(models.Model):
    velocidad = models.CharField(verbose_name="Velocidad", blank=True, null=True, max_length=50)
    
    class Meta:
        verbose_name = "Velocidad"
        verbose_name_plural = "Velocidades"
        ordering = ['id']
    
        
    def __str__(self):
        return self.velocidad