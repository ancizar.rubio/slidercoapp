from django.contrib import admin
from unidadMedida.models import Velocidad


class VelocidadAdmin(admin.ModelAdmin):
    readonly_fields =()

admin.site.register(Velocidad, VelocidadAdmin)