from django.db import models

# Create your models here.

class servicio(models.Model):
    nombre = models.CharField(verbose_name="Nombre", max_length=150)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edicion')
    
    class Meta:
        verbose_name = "servicio"
        verbose_name_plural = "servicios"
        ordering = ['id']
        
    def __str__(self):
        return self.nombre