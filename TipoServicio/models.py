from django.db import models

# Create your models here.

class TipoServicio(models.Model):
    nombre = models.CharField(max_length=100, verbose_name="Nombre")
    icono = models.CharField(verbose_name="icono", max_length=50)
    imagen = models.ImageField(verbose_name="imagen", upload_to="TipoServicio",blank=True, null=True, max_length=None)
    descripcion = models.TextField(verbose_name="Descripcion", max_length=500)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edicion')
    
    class Meta:
        verbose_name = "tiposervicio"
        verbose_name_plural = "tiposervicios"
        ordering = ['id']
        
    def __str__(self):
        return self.nombre