# Generated by Django 3.0.3 on 2020-02-26 04:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TipoServicio', '0003_tiposervicio_descripcion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tiposervicio',
            name='imagen',
            field=models.ImageField(blank=True, null=True, upload_to='TipoServicio', verbose_name='imagen'),
        ),
    ]
