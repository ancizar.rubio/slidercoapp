from rest_framework import serializers
from .models import TipoServicio


class TipoServicioSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = TipoServicio
        fields = ['id','nombre','icono','imagen','descripcion']