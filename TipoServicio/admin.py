from django.contrib import admin
from .models import TipoServicio

# Register your models here.

class TipoServicioAdmin(admin.ModelAdmin):
    readonly_fields =('created','updated')
    list_display = ('nombre', 'icono', 'imagen', 'descripcion')

admin.site.register(TipoServicio, TipoServicioAdmin)