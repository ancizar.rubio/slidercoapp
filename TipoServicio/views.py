from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import views
import rest_framework
from TipoServicio.serializers import TipoServicioSerializer
from TipoServicio.models import TipoServicio as TipoServicioModel

# Create your views here.


class TipoServicio(views.APIView):
    
    def get(self, request):
        queryset = TipoServicioModel.objects.all()
        serializer = TipoServicioSerializer(queryset, many=True)
        
        return Response(serializer.data)